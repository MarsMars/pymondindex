def get_all_complete_dwarfs_new(dwarfs, dwarfs_available, hats, level):
    level = level + 1
    print_context('BEGIN', dwarfs, dwarfs_available, hats, level)
    if is_nothing_to_do(dwarfs_available):
        if is_valid_result(dwarfs, hats):
            print_context('RESULT', dwarfs, dwarfs_available, hats, level)
            yield dwarfs
    else:
        try:
            set_obvious_dwarfs(dwarfs, dwarfs_available, hats)
        except ConflictError:
            pass
        else:
            if is_nothing_to_do(dwarfs_available):
                if is_valid_result(dwarfs, hats):
                    print_context('RESULT', dwarfs, dwarfs_available, hats, level)
                    yield dwarfs
            else:
                try:
                    key = dwarfs.index(None)
                except ValueError:
                    pass
                else:
                    print_context('key %s' % key, dwarfs, dwarfs_available, hats, level)
                    yield from set_dwarf_at_key_new(key, dwarfs, dwarfs_available, hats, level)


def get_dwarfs_list_new(hats):
    dwarfs = get_empty_dwarfs_list(len(hats))
    dwarfs_available = get_all_dwarfs_available(hats)
    try:
        set_corners(dwarfs, dwarfs_available, hats)
        set_obvious_dwarfs(dwarfs, dwarfs_available, hats)
        yield from get_all_complete_dwarfs_new(dwarfs, dwarfs_available, hats, 0)
    except ConflictError:
        pass


def set_left_neighbour_new(key, dwarfs_at_table, dwarfs_available, hats, level):
    yield from set_dwarf_at_key_new(key - 1, dwarfs_at_table, dwarfs_available, hats, level)


def set_right_neighbour_new(key, dwarfs_at_table, dwarfs_available, hats, level):
    yield from set_dwarf_at_key_new(key + 1, dwarfs_at_table, dwarfs_available, hats, level)


def set_dwarf_at_key_new(key, dwarfs, dwarfs_available, hats, level):
    print_context('key %s' % key, dwarfs, dwarfs_available, hats, level)
    for dwarf in dwarfs_available:
        da = dwarfs_available[:]
        da.remove(dwarf)
        dt = dwarfs[:]
        dt[key] = dwarf
        yield from get_all_complete_dwarfs_new(dt, da, hats, level)


def has_not_none_duplicates(numbers):
    numbers_wo_none = [n for n in numbers if n is not None]
    return len(numbers_wo_none) != len(set(numbers_wo_none))


def has_nones(items_list):
    return None in items_list


def get_empty_dwarfs_list(size):
    return [None] * size


def get_modulo_result(input_number):
    return input_number % 1000000007


def are_corners_in_conflict(dwarfs):
    return len(dwarfs) > 3 and dwarfs[1] == dwarfs[len(dwarfs) - 2]


def set_corners(dwarfs, dwarfs_available, hats):
    dwarfs[1] = hats[0]
    try:
        dwarfs_available.remove(dwarfs[1])
    except ValueError:
        pass
    if is_second_but_last_dwarfs_place_taken_by_someone_else(dwarfs, hats):
        raise ConflictError('Conflicting values')
    dwarfs[len(dwarfs) - 2] = hats[len(hats) - 1]
    if are_corners_in_conflict(dwarfs):
        raise ConflictError('Conflicting values')
    try:
        dwarfs_available.remove(dwarfs[len(hats) - 2])
    except ValueError:
        pass


def is_second_but_last_dwarfs_place_taken_by_someone_else(dwarfs_at_table, hats):
    return not is_second_but_last_dwarf_none(dwarfs_at_table) and dwarfs_at_table[len(hats) - 2] != hats[len(hats) - 1]


def is_second_but_last_dwarf_none(dwarfs_at_table):
    return dwarfs_at_table[len(dwarfs_at_table) - 2] is None


class ConflictError(Exception):
    pass


def get_all_dwarfs_available(hats):
    return list(range(1, len(hats) + 1))


def print_context(msg, dwarfs_at_table, dwarfs_available, hats, level):
    if True:
        print()
        print('level %s:%s' % (level, msg))
        print('level %s:dwarfs_at_table%s' % (level, dwarfs_at_table))
        print('level %s:dwarfs_available%s' % (level, dwarfs_available))
        # print('level %s:hats%s' % (level, hats))


def is_nothing_to_do(dwarfs_available):
    return len(dwarfs_available) == 0


def are_hats_matching(dwarfs, hats):
    for key in range(0, len(hats)):
        if hats[key] not in get_neighbours(dwarfs, hats, key):
            return False
    return True


def get_neighbours(dwarfs, hats, key):
    neighbours = []
    if key > 0:
        neighbours.append(dwarfs[key - 1])
    if key < len(hats) - 1:
        neighbours.append(dwarfs[key + 1])
    return neighbours


def is_valid_result(dwarfs, hats):
    return not has_not_none_duplicates(dwarfs) and are_hats_matching(dwarfs, hats)


def is_edge(key, dwarfs_at_table):
    return is_left_edge(key, dwarfs_at_table) or is_right_edge(key, dwarfs_at_table)


def is_left_edge(key, dwarfs_at_table):
    return key == 0


def is_right_edge(key, dwarfs_at_table):
    return key == (len(dwarfs_at_table) - 1)


def is_dwarf_in_place(key, dwarfs_at_table):
    return dwarfs_at_table[key] is not None


def get_dwarfs_if_complete(dwarfs):
    if not has_not_none_duplicates(dwarfs):
        yield dwarfs


def set_obvious_dwarfs(dwarfs, dwarfs_available, hats):
    set_corners_with_two_neighbours(dwarfs, dwarfs_available, hats)
    set_dwarf_when_two_neighbours_agree(dwarfs, dwarfs_available, hats)


def set_dwarf_when_two_neighbours_agree(dwarfs, dwarfs_available, hats):
    for key in range(1, len(hats) - 1):
        if not is_dwarf_at_key(dwarfs, key) and both_neighbours_equal(hats, key):
            dwarfs[key] = hats[key - 1]
            try:
                dwarfs_available.remove(dwarfs[key])
            except ValueError:
                raise ConflictError('Conflicting values')


def both_neighbours_equal(hats, key):
    return hats[key - 1] == hats[key + 1]


def is_dwarf_at_key(dwarfs, key):
    return dwarfs[key] is not None


def set_corners_with_two_neighbours(dwarfs, dwarfs_available, hats):
    if len(hats) > 2:
        set_left_corner_with_two_neighbours(dwarfs, dwarfs_available, hats)
        set_right_corner_with_two_neighbours(dwarfs, dwarfs_available, hats)


def set_right_corner_with_two_neighbours(dwarfs, dwarfs_available, hats):
    if not is_dwarf_at_key(dwarfs, len(hats) - 1) and is_dwarf_at_key(dwarfs, len(hats) - 3):
        if dwarfs[len(hats) - 3] != hats[len(hats) - 2]:
            dwarfs[len(hats) - 1] = hats[len(hats) - 2]
            try:
                dwarfs_available.remove(dwarfs[len(hats) - 1])
            except ValueError:
                raise ConflictError('Conflicting values')


def set_left_corner_with_two_neighbours(dwarfs, dwarfs_available, hats):
    if not is_dwarf_at_key(dwarfs, 0) and is_dwarf_at_key(dwarfs, 2):
        if dwarfs[2] != hats[1]:
            dwarfs[0] = hats[1]
            try:
                dwarfs_available.remove(dwarfs[0])
            except ValueError:
                raise ConflictError('Conflicting values')
