import unittest
from parameterized import parameterized
import dwarfparty


class DwarfsPartyChecker(unittest.TestCase):

    @parameterized.expand([[[None], 1],
                           [[None, None], 2]])
    def test_empty_list(self, empty_list, list_size):
        self.assertEqual(empty_list, dwarfparty.get_empty_dwarfs_list(list_size))

    @parameterized.expand([[False, [1, 2]],
                           [True, [1, 1]]])
    def test_has_not_none_duplicates(self, has, dwarfs):
        self.assertEqual(has, dwarfparty.has_not_none_duplicates(dwarfs))

    @parameterized.expand([[[2, 1], [], [1, 2]],
                           [[None, 1, None], [2, 3], [1, 2, 1]],
                           [[None, 1, 3, None], [2, 4], [1, 2, 1, 3]]])
    def test_set_corners(self, dwarfs_expected, dwarfs_available_expected, hats):
        dwarfs = dwarfparty.get_empty_dwarfs_list(len(hats))
        dwarfs_available = dwarfparty.get_all_dwarfs_available(hats)
        dwarfparty.set_corners(dwarfs, dwarfs_available, hats)
        self.assertEqual(dwarfs_expected, dwarfs)
        self.assertEqual(dwarfs_available_expected, dwarfs_available)

    @parameterized.expand([[[1, 2, 3]], [[1, 2, 3, 1]]])
    def test_set_corners_throws_exception_on_conflict(self, hats):
        with self.assertRaises(dwarfparty.ConflictError) as context:
            dwarfparty.set_corners(dwarfparty.get_empty_dwarfs_list(len(hats)),
                                   dwarfparty.get_all_dwarfs_available(hats), hats)
        self.assertTrue('Conflicting values' in context.exception.args)

    @parameterized.expand([[[[2, 1]], [1, 2]],
                           [[[1, 2]], [2, 1]],
                           [[], [1, 1]],
                           [[[2, 1, 3], [3, 1, 2]], [1, 2, 1]],
                           [[], [1, 2, 3]],
                           [[[1, 2, 3], [3, 2, 1]], [2, 3, 2]],
                           [[[1, 2, 3, 4]], [2, 1, 2, 3]],
                           [[[4, 2, 3, 1]], [2, 4, 1, 3]],
                           [[], [1, 2, 3, 1]],
                           [[[3, 1, 2, 5, 4], [4, 1, 2, 5, 3]], [1, 2, 1, 2, 5]],
                           [[[2, 3, 4, 1, 5], [5, 3, 4, 1, 2]], [3, 4, 3, 4, 1]]])
    def test_list_generator(self, dwarfs_lists_expected, hats):
        dwarfs_lists = []
        for dwarfs in dwarfparty.get_dwarfs_list_new(hats):
            dwarfs_lists.append(dwarfs)
        self.assertEqual(dwarfs_lists_expected, dwarfs_lists)

    def test_5(self):
        dwarfs_lists = []
        for dwarfs in dwarfparty.get_dwarfs_list_new([3, 4, 3, 4, 1]):
            if dwarfs in dwarfs_lists:
                continue
            dwarfs_lists.append(dwarfs)
        self.assertEqual([[2, 3, 4, 1, 5], [5, 3, 4, 1, 2]], dwarfs_lists)

    def test_set_dwarf_when_two_neighbours_agree(self):
        dwarfs_expected = [None, 1, 2, 5, None]
        dwarfs_available_expected = [3, 4]
        hats = [1, 2, 1, 2, 5]
        dwarfs = dwarfparty.get_empty_dwarfs_list(len(hats))
        dwarfs_available = dwarfparty.get_all_dwarfs_available(hats)
        dwarfparty.set_corners(dwarfs, dwarfs_available, hats)
        dwarfparty.set_dwarf_when_two_neighbours_agree(dwarfs, dwarfs_available, hats)
        self.assertEqual(dwarfs_expected, dwarfs)
        self.assertEqual(dwarfs_available_expected, dwarfs_available)

    @parameterized.expand([[[2, 1, 3], [None, 1, 3], [2], [1, 2, 1]],
                           [[3, 1, 2], [3, 1, None], [2], [1, 2, 1]],
                           [[2, 1, 3, 5, 4], [None, 1, 3, 5, None], [2, 4], [1, 2, 1, 4, 5]]])
    def test_set_corners_with_two_neighbours(self, dwarfs_expected, dwarfs, dwarfs_available,
                                             hats):
        dwarfparty.set_corners(dwarfs, dwarfs_available, hats)
        dwarfparty.set_corners_with_two_neighbours(dwarfs, dwarfs_available, hats)
        self.assertEqual(dwarfs_expected, dwarfs)
        self.assertEqual([], dwarfs_available)

    @parameterized.expand([[1, 1],
                           [2, 2],
                           [7, 7],
                           [8, 8],
                           [0, 1000000007],
                           [1, 1000000008],
                           [0, 2000000014]])
    def test_modulo_thing(self, result, input_number):
        self.assertEqual(result, dwarfparty.get_modulo_result(input_number))

    def test_6(self):
        dwarfs_lists = []
        for dwarfs in dwarfparty.get_dwarfs_list_new([2, 3, 2, 3, 4, 5]):
            dwarfs_lists.append(dwarfs)
        self.assertEqual([[1, 2, 3, 4, 5, 6], [1, 2, 3, 6, 5, 4], [6, 2, 3, 1, 5, 4], [6, 2, 3, 4, 5, 1]], dwarfs_lists)

    @parameterized.expand([[[1, 2, 3], [2, 1, 2], True],
                           [[2, 1, 3], [2, 1, 2], False],
                           [[1, 3, 2], [2, 1, 2], False],
                           [[1, 2], [2, 1], True],
                           [[1], [1], False],
                           [[1, 3, 2, 4], [3, 1, 3, 2], True],
                           [[1, 3, 2, 4], [3, 1, 1, 2], False],
                           [[1, 3, 2, 4, 5], [3, 1, 3, 2, 4], True],
                           [[1, 2, 2, 4, 5], [3, 1, 3, 2, 4], False],
                           [[1, 3, 2, 4, 5], [3, 1, 3, 2, 5], False]])
    def test_match_hats(self, dwarfs, hats, match):
        self.assertEqual(match, dwarfparty.are_hats_matching(dwarfs, hats))


if __name__ == "__main__":
    unittest.main()
