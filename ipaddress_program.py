def insert_dots_between(ip_string):
    ip_address = ''
    if len(ip_string) > 4:
        ip_address = ip_address + ip_string[0] + ip_string[1] + '.'
        for x in range(2, len(ip_string)):
            ip_address = ip_address + ip_string[x] + '.'
        return ip_address[:-1]
    return insert_dots_between_segments(split_string_to_segments(ip_string))


def get_ip_address_from_string(ip_string):
    return insert_dots_between(ip_string)


def insert_dots_between_segments(segments):
    return '.'.join(segments)


def split_string_to_segments(ip_string):
    return list(ip_string)


def split_string_to_four_segments(ip_string):
    segments = split_string_to_segments(ip_string)
    if len(ip_string) < 5:
        return segments
    return [[''.join(segments[:2])] + segments[2:]]


def is_one_byte_number(number):
    return 0 <= number <= 255


def has_heading_zero(segment):
    return len(segment) > 1 and segment[0] == '0'


def is_segment_valid(segment):
    if not segment.isnumeric() or not is_one_byte_number(int(segment)) or has_heading_zero(segment):
        return False
    return True


def is_ip_string_valid(ip_string):
    if not ip_string.isnumeric() or not is_ip_string_length_correct(ip_string):
        return False
    return True


def is_ip_string_length_correct(ip_string):
    return 4 <= len(ip_string) <= 12


def can_split_to_segments(ip_string, number_of_segments):
    return len(ip_string) >= number_of_segments


def split_to_number_of_segments(ip_string, number_of_segments):
    if not is_number_of_segments_valid(number_of_segments):
        raise ValueError('Invalid number of segments: %i' % number_of_segments)
    if not can_split_to_segments(ip_string, number_of_segments):
        return []
    if number_of_segments == 1:
        return ip_string
    if len(ip_string) == number_of_segments:
        return [insert_dots_between_segments(list(ip_string))]
    if number_of_segments == 2:
        return split_to_two_part(ip_string)
    result = []
    for x in range(len(ip_string) - 1):
        head = get_head(ip_string, x)
        tail = get_tail(ip_string, x)
        all_tails = split_to_number_of_segments(tail, number_of_segments - 1)
        for t in all_tails:
            result.append(join_head_and_tail(head, t))
    return result


def split_to_two_part(ip_string):
    ip_addresses = []
    for x in range(len(ip_string) - 1):
        ip_addresses.append(join_head_and_tail(get_head(ip_string, x), get_tail(ip_string, x)))
    return ip_addresses


def join_head_and_tail(head, tail):
    return dot_join([head, tail])


def get_tail(ip_string, position):
    return ip_string[position + 1:]


def get_head(ip_string, position):
    return ip_string[:position + 1]


def dot_join(parts):
    return '.'.join(parts)


def is_number_of_segments_valid(number_of_segments):
    return number_of_segments > 0


def split_to_numbers_of_segments_with_letters(letters_string, number_of_segments):
    if not is_number_of_segments_valid(number_of_segments):
        raise ValueError('Invalid number of segments: %i' % number_of_segments)
    if not can_split_to_segments(letters_string, number_of_segments):
        return []
    if number_of_segments == 1:
        return letters_string
    if len(letters_string) == number_of_segments:
        return [insert_dots_between_segments(list(letters_string))]
    if number_of_segments == 2:
        return split_to_two_part(letters_string)
    result = []
    for x in range(len(letters_string) - 1):
        head = get_head(letters_string, x)
        tail = get_tail(letters_string, x)
        all_tails = split_to_number_of_segments(tail, number_of_segments - 1)
        for t in all_tails:
            result.append(join_head_and_tail(head, t))
    return result


def ok_ip(ip):
    ipm = ip + '.'
    segments = []
    s = ""
    for x in ipm:
        if x == '.':
            segments.append(s)
            s = ""
        else:
            s += x
    for y in segments:
        if len(y) > 1 and y[0] == '0':
            return False
        if int(y) > 255:
            return False
    return True


def validator(ip_list):
    result = []
    for x in ip_list:
        if ok_ip(x):
            result.append(x)
    return result
