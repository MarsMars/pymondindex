import unittest
from parameterized import parameterized
import ipaddress_program


class IpAddressCheckerTestCase(unittest.TestCase):

    @parameterized.expand([['1111', '1.1.1.1'], ['1112', '1.1.1.2'], ['1113', '1.1.1.3'], ['1114', '1.1.1.4']])
    def test_string_to_address_conversion_for_single_digit_addresses(self, ip_string, ip_address):
        self.assertEqual(ip_address, ipaddress_program.get_ip_address_from_string(ip_string))

    @parameterized.expand([['10111', '10.1.1.1']])
    def test_string_to_address_conversion_for_two_digits_in_front(self, ip_string, ip_address):
        self.assertEqual(ip_address, ipaddress_program.get_ip_address_from_string(ip_string))

    def test_split_string_to_four_segments(self):
        self.assertEqual([['aa', 'b', 'c', 'd']], ipaddress_program.split_string_to_four_segments('aabcd'))

    @parameterized.expand([['0', True],
                           ['1', True],
                           ['255', True],
                           ['-1', False],
                           ['a', False],
                           ['256', False],
                           ['01', False],
                           ['001', False],
                           ['000', False]])
    def test_valid_segments(self, segment, valid_or_not):
        self.assertEqual(valid_or_not, ipaddress_program.is_segment_valid(segment))

    @parameterized.expand([['0000', True],
                           ['A', False],
                           ['000', False],
                           ['1111111111111', False]])
    def test_valid_string(self, ip_string, valid_or_not):
        self.assertEqual(valid_or_not, ipaddress_program.is_ip_string_valid(ip_string))

    @parameterized.expand([['1', 0],
                           ['1', -1],
                           ['1', -2]])
    def test_segments_splitting_exceptions(self, ip_string, number_of_segments):
        with self.assertRaises(ValueError) as context:
            ipaddress_program.split_to_number_of_segments(ip_string, number_of_segments)
        self.assertTrue(('Invalid number of segments: %i' % number_of_segments) in context.exception.args)

    @parameterized.expand([['', 1],
                           ['1', 1],
                           ['1', 2],
                           ['1', 3],
                           ['1', 4],
                           ['1', 5],
                           ['11', 1]])
    def test_segments_splitting_throws_no_exceptions(self, ip_string, number_of_segments):
        try:
            ipaddress_program.split_to_number_of_segments(ip_string, number_of_segments)
        except ValueError:
            self.fail('split_to_number_of_segments raised unexpected ValueError')

    @parameterized.expand([['', 1, []],
                           ['', 2, []],
                           ['1', 2, []],
                           ['11', 2, ['1.1']],
                           ['111', 2, ['1.11', '11.1']],
                           ['222', 2, ['2.22', '22.2']],
                           ['1111', 2, ['1.111', '11.11', '111.1']],
                           ['55555', 2, ['5.5555', '55.555', '555.55', '5555.5']],
                           ['4444', 3, ['4.4.44', '4.44.4', '44.4.4']]])
    def test_segments_splitting(self, ip_string, number_of_segments, segments):
        self.assertEqual(segments, ipaddress_program.split_to_number_of_segments(ip_string, number_of_segments))

    @parameterized.expand([['', 1, []],
                           ['', 2, []],
                           ['a', 2, []],
                           ['aa', 2, ['a.a']],
                           ['aaa', 2, ['a.aa', 'aa.a']],
                           ['bbb', 2, ['b.bb', 'bb.b']],
                           ['aaaa', 2, ['a.aaa', 'aa.aa', 'aaa.a']],
                           ['ccccc', 2, ['c.cccc', 'cc.ccc', 'ccc.cc', 'cccc.c']],
                           ['dddd', 3, ['d.d.dd', 'd.dd.d', 'dd.d.d']]])
    def test_of_letters(self, letters_string, number_of_segments, segments):
        self.assertEqual(segments,
                         ipaddress_program.split_to_numbers_of_segments_with_letters(letters_string,
                                                                                     number_of_segments))

    @parameterized.expand([['192.168.1.1', 1],
                           ['192.068.1.1', 0],
                           ['1.90.24.26', 1],
                           ['19.0.242.6', 1],
                           ['19.256.1.1', 0],
                           ['0.0.0.0', 1],
                           ['19.256.1.-1', 0]])
    def test_of_ok_ip_function(self, ip, result):
        self.assertEqual(result, ipaddress_program.ok_ip(ip))

    @parameterized.expand(
        [[['1.90.24.26', '1.90.242.6', '19.0.24.26', '19.02.42.6'], ['1.90.24.26', '1.90.242.6', '19.0.24.26']]])
    def test_validator(self, ip_list, result):
        self.assertEqual(result, ipaddress_program.validator(ip_list))


if __name__ == '__main__':
    unittest.main()
