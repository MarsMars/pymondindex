import unittest
from mon import mont


class MyTestCase(unittest.TestCase):
    def test_result_of_function_mont(self):
        self.assertEqual(mont(5, 4, 5, 2, 4, 5, 3, 1, 9, 3, 10), "3\n2 3\n4 7\n5 8")  # add assertion here


if __name__ == '__main__':
    unittest.main()
